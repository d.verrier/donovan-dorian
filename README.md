# donovan-dorian

* [X] Autotools 
* [X] Diagramme de classe
* [X] Declarer les classes .h
* [X] Implementer les classes .h
* [X] Declarer les classes .cpp
* [X] Implementer les classes .cpp
* [X] Faire la classe CLI
* [ ] Faire la classe main.cpp
* [ ] Modifier le schema UML