#ifndef ALBUM_H
#define ALBUM_H
#include <iostream>
#include <chrono>
using namespace std;

/**
 * \class album
 * \brief definition of the class album
 */
class album
{
 public:

  /**
   * \brief default constructor of album
   */
  album();

  /**
   * \brief constructor with parameters of album
   * \param id the identifier of an album
   * \param name the name of an album
   * \param date the date of an album
   */
  album(int, string, chrono::system_clock);

  /**
   * \brief allows reading an identifier of an album
   * \return int representing the id of an album
   */
  int getId();

  /**
   * \brief allows reading a name of an album
   * \return string representing the name of an album
   */
  string getName();

  /**
   * \brief allows writting a name of an album
   */
  void setName(string);

  /**
   * \brief allows reading a date of an album
   * \return system_clock representing the date of an album
   */
  chrono::system_clock getDate();

  /**
   * \brief allows writting a date of an album
   */
  void setDate(chrono::system_clock);

 private:
  int id;///< Attribute defining the id of an album
  string name;///< Attribute defining the name of an album
  chrono::system_clock date;///< Attribute defining the date of an album
};

#endif // ALBUM_H
