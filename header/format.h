#ifndef FORMAT_H
#define FORMAT_H
#include <iostream>
using namespace std;

/**
 * \class format
 * \brief definition of the class format
 */
class format
{
 public:

  /**
   * \brief default constructor of format
   */
  format();

  /**
   * \brief constructor with parameters of format
   * \param id the identifier of a genre
   * \param type the type of a genre
   */
  format(int, string);

  /**
   * \brief allows reading an identifier of a format
   * \return int representing the id of a format
   */
  int getId();

  /**
   * \brief allows reading a type of a format
   * \return string representing the type of a format
   */
  string getType();

  /**
   * \brief allows writting a type of a format
   */
  void setType(string);

 private:
  int id;///< Attribute defining the id of a format
  string type;///< Attribute defining the type of a format
};

#endif // FORMAT_H
