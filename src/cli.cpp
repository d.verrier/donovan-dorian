#include "../header/cli.h"

DEFINE_uint64(duration, 4000, "Duration playlist");
DEFINE_string(genre, "", "Genre of the music");
DEFINE_string(type, "", "Format of the playlist");
DEFINE_string(name, "", "Title of the music");
DEFINE_string(playlist, "", "Name of the playlist");
DEFINE_string(artist, "", "Artist name of the music");
DEFINE_string(album, "", "Album Name");
