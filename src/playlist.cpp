#include "../header/playlist.h"
#include "../header/track.h"

#include <fstream>

playlist::playlist() :
  id(-1),
  title("playlist"),
  duration(0)
{}

playlist::playlist(int _id,
                   string _title,
                   vector<track> _Tracks,
                   int _duration ) :
  id(_id),
  title(_title),
  Tracks(_Tracks),
  duration(_duration)
{}

int playlist::getId()
{
  return id;
}

std::string playlist::getTitle()
{
  return title;
}

void playlist::setTitle(std::string _title)
{
  title = _title;
}

int playlist::getDuration()
{
  return duration;
}

void playlist::setDuration(int _duration)
{
  duration = _duration;
}


void playlist::writeM3U()
{
  std::string filename = title + ".m3u";
  {
    std::ofstream m3u_stream(filename, std::ios::out);

    if(m3u_stream.is_open())
    {
      for(track current_track : Tracks)
      {
        m3u_stream << current_track.getPath() << std::endl;
      }
    }
  }
}
