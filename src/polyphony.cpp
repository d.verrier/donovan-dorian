#include "../header/polyphony.h"
#include <iostream>

using namespace std;

polyphony::polyphony():
  polyphony(0, 0)
{}

polyphony::polyphony(int _id, int _number):
  id(_id),
  number(_number)
{}

int polyphony::getId()
{
  return id;
}

int polyphony::getNumber()
{
  return number;
}

void polyphony::setNumber(int _number)
{
  number = _number;
}
